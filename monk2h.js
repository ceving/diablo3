function monk2h ()
{
  return fetch ('https://eu.battle.net/d3/de/item/two-handed/')

  // Take the text from the respons.
    .then (r => r.text())

  // Use the text to build DOM tree.
    .then (t => (new DOMParser()).parseFromString (t, 'text/html'))

  // Extract the links and return a generator of hrefs.
    .then (d => function* () {
      for (let a of d.querySelectorAll ('li.open a'))
        if (!a.href.match (/flail|mighty-weapon|scythe/g))
          yield a.href;
    })

  // Fetch each href, extract the item attributes and return a
  // generator of generators of items.
    .then (urls => function* () {
      for (let u of urls()) {
        yield fetch (u)
          .then (r => r.text())
          .then (t => (new DOMParser()).parseFromString (t, 'text/html'))
          .then (d => function* () {
            for (let r of d.querySelectorAll ('tbody tr.crafted:not(.no-results)')) { // table row
              let i = {}; // item
              if (r.classList.contains('legendary'))
                i.quality = 'l';
              else
                i.quality = 'r';
              let n = r.querySelector('.item-details-text a');
              i.name = n.textContent.trim();
              i.color = window.getComputedStyle(n).getPropertyValue('color');
              i.type = r.querySelector('.item-type').textContent.trim();
              let d = r.querySelector('.item-weapon-damage p').textContent.trim(); // damage
              i.damage = /^(\d+)-(\d+) .*/.exec(d).slice(1,3).map(Number);
              i.damage_sum = i.damage.reduce((s,x) => s+x, 0);
              i.level = Number(r.querySelector('.column-level').textContent);
              if (i.level > 1)
                yield i;
            }
          });
      }
    })

  // Flatten the promise of a generator of promises of generators of items
    .then (pg => Promise.all([...pg()].map (p => p.then (ig => [...ig()]))))
    .then (a => a.reduce((a, e) => a.concat(e), []))

  // Select items with highes damage per level. Remove items with a
  // higer level but lower damage
    .then (items => {
      let grouped = {r: {}, l: {}};
      for (let i of items) {
        if (!grouped[i.quality][i.level] || grouped[i.quality][i.level].damage_sum < i.damage_sum)
          grouped[i.quality][i.level] = i;
      }
      let grouped2 = {r: {}, l: {}};
      for (let q of ['r', 'l']) {
        let k = Object.keys(grouped[q]);
        let j = 0;
        let i = 0;
        grouped2[q][k[j]] = grouped[q][k[i]];
        for (++i; i < k.length; i++) {
          if (grouped2[q][k[j]].damage_sum < grouped[q][k[i]].damage_sum) {
            j++
            grouped2[q][k[j]] = grouped[q][k[i]];
          }
        }
      }
      return grouped2;
    })

  // Format
    .then (items => {
      let t = document.createElement('table');
      for (let q of ['r', 'l']) {
        let b = document.createElement('tbody');
        t.appendChild(b);
        for (let i in items[q]) {
          let r = document.createElement('tr');
          b.appendChild(r);
          let d;
          for (let a of ['level', 'type', 'name']) {
            r.appendChild(d = document.createElement('td'));
            d.appendChild(document.createTextNode(items[q][i][a]));
            d.style.color = items[q][i].color;
            d.style.padding = '0 1ex';
          }
          r.appendChild(d = document.createElement('td'));
          d.appendChild(document.createTextNode(items[q][i].damage[0] + '-' + items[q][i].damage[1]));
          d.style.color = items[q][i].color;
          d.style.padding = '0 1ex';
        }
      }
      console.log(t.outerHTML);
    });
}
